//
//  DZWebBroker.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk Simovic on 2.3.22..
//  Copyright © 2021 RedCellApps. All rights reserved..
//

import Foundation
#if !os(macOS)
import UIKit
#endif
import Reachability

protocol WebBrokerProtocol {
    func connectionSuccessful()
    func connectionInterrupted()
    func responseRetrunToRetry(retry_msg : String)
}

//public var ssessionID = String()
//public var ssessionIDFlag = Bool()
//public var srequestID = String()

public class DZWebBroker : DZBaseWebBroker {
    public static let shared = DZWebBroker()
    //var reachability = Reachability()!
    var webBrokerDelegate : WebBrokerProtocol?
    
    var sessionID = String()
    var unsentJsonObjectArray = [[String: Any]]()
    
//    var brokerURL:String            = "https://stagingbroker.datazoom.io/broker/v1/logs" salje evente
//    var brokerURL:String            = "https://app.datazoom.io/broker/v1/logs"
    
    var configPath: String          = "/beacon/v1/config?configuration_id="
    
    let defaultBrokerUrl: String    = "https://stagingbroker.datazoom.io/broker/v1/logs"
    var brokerUrl: String           = ""
    
    let epochUrl: String            = "https://stagingbroker.datazoom.io/broker/v1/getEpochMillis"
    
    var networkDetailsUrl: String  = "https://pro.ip-api.com/json/?key=ZP6KtPdtCCRcgGk"
    var automationUrl: String      = "https://stagingplatform.datazoom.io/qa-automation/v1/collector-logs/"
    
    var customerCode:String         = String()
    var connectorList:String        = String()
    var configurationId:String      = String()
    var oAuthToken:String           = String()
    
    public var eventList            = [String]()
    public var metaDataList         = [String]()
    public var fluxDataList         = [String]()
    public var interval             = Int()
    
    public var eventTypesVersion    = String()
    public var eventContentList     = [String]()
    public var eventAdList          = [String]()
    
    public var networkDetails: DZConfigNetworkDetails? = nil
    
    public var serverTime = 0
    public var localTime = 0
    public var serverTimeOffset = 0
    
    var runId:String = String()
    
    
    func getCustomerCode()->String {
        return self.customerCode
    }
    
    func getConnectorList()->String {
        return self.connectorList
    }
    
    func getOAuthToken()->String {
        return self.oAuthToken
    }
    
    func getConfigurationId()->String {
        return self.configurationId
    }
    
    //initialise
    func initialise(configId: String, connectURL:String, onCompletion: @escaping (Bool,Error?) -> Void) {
        print ("%%%%%%%%%%%% Reachability connection: \(reachability.connection) %%%% \n")
        configurationId = configId
        if reachability.connection != .unavailable {
            getConfigurationRequest(configurationId: configId, configurationUrl:connectURL, onCompletion: {(suceess, error) in
                if suceess == true {
                    self.getServerTimeOffset{ (suceess, error) in
                        if suceess == true {
                            onCompletion(true,nil)
                        } else {
                            onCompletion(false, error!)
                        }
                    } 
                } else {
                    onCompletion(false,error!)
                }
            })
        } else {
            print("%%%% Error %%%% \n")
        }
    }
    
    func getConfigurationRequest(configurationId: String, configurationUrl:String, onCompletion: @escaping (Bool,Error?) -> Void) {
        if(self.configurationId != ""){
            let configurationFullUrl = "\(configurationUrl)\(self.configPath)\(configurationId)"

#warning("This should be commented, but server sometimes returning nil")
            self.brokerUrl = self.defaultBrokerUrl
            
            getTheAPIToWork(method: "get", connectionURL: configurationFullUrl) { (response, error) in
                do {
                    let result = try JSONDecoder().decode(DZConfig.self, from: response!)
                    
                    // get broker url
#warning("This should be uncommented, but server sometimes returning nil")
//                    self.brokerUrl = self.defaultBrokerUrl // todo  result.broker?.url ?? self.defaultBrokerUrl
                    
                    // get customer data
                    self.customerCode = result.customerCode!
                    self.connectorList = result.connectorList!
                    self.configurationId = result.configurationId!
                    
                    // get event types
                    let eventTypeList = result.events!.types
//                    for oneValue in newComp! {
//                        self.eventList.append(oneValue.name!)
//                    }
                    self.eventList = eventTypeList != nil ? eventTypeList!.map { $0.name }.compactMap { $0 } : []
                    self.eventTypesVersion = "v2"
                    
                    if result.eventsV3 != nil {
                        let eventTypeV3 = result.eventsV3
                        if  eventTypeV3!.typesContent?.count ?? 0  > 0 || eventTypeV3!.typesAd?.count ?? 0 > 0 {
                            self.eventContentList = eventTypeV3!.typesContent?.map { $0.name }.compactMap { $0 } ?? []
                            self.eventAdList = eventTypeV3!.typesAd?.map { $0.name }.compactMap { $0 } ?? []
                            self.eventTypesVersion = "v3"
                        }
                    }
                    
                    // get metadata list
                    self.metaDataList = result.events!.metdata!
                    
                    // get flux list
                    self.fluxDataList = result.events!.flux_data!
                    
                    // get interval
                    self.interval = result.events?.interval ?? 30000
                    
//                    DZConnector.connector.removeTimer()
                    onCompletion(true,nil)
                } catch {
                    print("%%%% Config File Request Error: %%%% \n \(error)")
                    onCompletion(false,error)
                }
            }
        }
        else {
            print("%%%% No configuration url define %%%% \n")
            
        }
    }
    
    func getServerTimeOffset(onCompletion: @escaping (Bool,Error?) -> Void) {
        let dateNow = NSDate()
        self.localTime = Int(dateNow.timeIntervalSince1970 * 1000)
        getTheAPIToWork(method: "get", connectionURL:  "\(self.epochUrl)") { (response, error) in
            do {
                let offsetResponse = try JSONSerialization.jsonObject(with: response!, options: [])
                guard let jsonArr = offsetResponse as? [String:Any] else {
                    return
                }
                guard let servTime = jsonArr["epoch_millis"] as? Int else {return}
                self.serverTimeOffset = servTime - self.localTime - ((servTime - self.localTime) / 2)
                
                onCompletion(true,nil)
            } catch {
                print("%%%% Server Time Offset Request Error: %%%% \n \(error)")
                onCompletion(false,error)
            }
        }
    }
    
    func send(_ value: String, onSuccess: @escaping (String)-> Void) {
        sendEventToAPI (method: "post", parameter: value, connectionURL: "\(self.brokerUrl)") { (response, error) in
            do {
                if response != nil {
                    print ("%%%%%%%%%%%% Sent data: %%%% \n\(value)")
                    let _ = try JSONDecoder().decode(DZEventResponse.self, from: response!)
                    print("%%%% API Response: %%%% \n \(String(describing: String(data: response!, encoding: .utf8)))")
                }
            }
            catch let error {
                print ("%%%%%%%%%%%% Error: %%%% \n \(error)")
            }
        }
    }
    
    func getNetworkDetails(_ value: String?, onSuccess: @escaping (Bool)-> Void){
        getTheAPIToWork(method: "get", connectionURL: self.networkDetailsUrl) {  (response, error) in
            if response != nil  {
                do {
                    self.networkDetails = try JSONDecoder().decode(DZConfigNetworkDetails.self, from: response!)
                    onSuccess(true)
                } catch {
                    onSuccess(false)
                }
            } else {
                print("%%%% Network Details Error: %%%% \n \(String(describing: error?.localizedDescription))")
                onSuccess(false)
            }
        }
    }
    
}

//MARK:- Automation
extension DZWebBroker {
    /**************** QA Automation ****************/
    //?!?!?!?!?!?!?!?!?!?!?!?!!?!?!?!?!?!?!?!?!?!?//
    
    func getRunIDForAutomation(onCompletion: @escaping (Bool,Error?) -> Void) {
        
        getTheAPIToWork(method: "get", connectionURL: "\(self.automationUrl)collector-runid") { (response, error) in
            if response != nil {
                let backToString = String(data: response!, encoding: String.Encoding.utf8) as String?
                print(">>>>> get RunID For Automation ::: \(String(describing: backToString))")
                do {
                    let json = try JSONSerialization.jsonObject(with: response!, options: JSONSerialization.ReadingOptions.allowFragments)  as! [String:String]
                    if let runID = json["runId"]   {
                        self.runId = runID
                    }
                    onCompletion(true,nil)
                } catch {
                    onCompletion(false,error)
                    print("%%%% Run ID For Automation Error: %%%% \n \(error)")
                }
            } else {
                print("%%%% Run ID For Automation Error: %%%% \n \(String(describing: error?.localizedDescription))")
                
            }
        }
        
    }
    
    
    func postMessageForAutomation(_ parameter: [String : Any], onSuccess: @escaping (String?)-> Void){
        if self.runId != "" {
            let url = "\(self.automationUrl)collector-logs/\(self.runId)"
            print("::::::: Automation URL ::  \(url) :::::::")
            getThePostAPIToWork(method: "post", parameter: parameter, connectionURL: url) { (response,error) in
                //_ = String(data: response, encoding: String.Encoding.utf8) as String?
                if response != nil  {
                    do {
                        let json = try JSONSerialization.jsonObject(with: response!, options: .allowFragments) as! [String:String]
                        print("%%%% Automation response: %%%% \n -- \(json)")
                        onSuccess("")
                    } catch {
                        onSuccess(String(describing: error.localizedDescription))
                        print("%%%% Post Message For Automation Error: %%%% \n \(error)")
                    }
                } else {
                    onSuccess(String(describing: error?.localizedDescription))
                    print("%%%% Post Message For Automation Error: %%%% \n \(String(describing: error?.localizedDescription))")
                }
            }
        } else {
            onSuccess(nil)
        }
    }
}
