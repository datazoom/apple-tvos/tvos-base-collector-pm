//
//  DZCollectorProtocol.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk Simovic on 2.3.22..
//  Copyright © 2021 RedCellApps. All rights reserved..
//

import Foundation

public protocol DZCollectorProtocol {
    func triggerNotImplemented(_ onSent: (() -> Void)?)
    
    func triggerDatazoomLoaded(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerError(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerMilestone(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerRenditionChange(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerMediaRequest(_ onBeforeSend: (() -> Void)?, _ onCreate: ((DZEvent) -> Void)?, _ onSent: (() -> Void)?)
    func triggerMediaLoaded(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerPlaybackReady(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerPlaybackStart(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerPlay(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerPlaying(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerSeekStart(_ playHeadPos: Float,_ fromValue: Int, _ toValue: Int, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerSeekEnd(_ playHeadPos: Float,_ fromValue: Int, _ toValue: Int, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerPlaybackComplete(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerHeartbeat(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerBufferStart(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerBufferEnd(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerStallStart(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerStallEnd(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerPause(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerResume(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    
    func triggerNetworkTypeChange(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerNetworkTimed(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerVolumeChange(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerMute(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerUnmute(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerResize(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerFullScreen(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerExitFullScreen(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerAudioTrackChanged(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerQualityChangeRequest(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerSubtitleChange(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerCastStart(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerCastEnd(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
    func triggerPlayBtn(_ onBeforeSend: (() -> Void)?, _ onSent: (() -> Void)?)
}


public extension DZCollectorProtocol {
    func sendMessageForEvent(_ eventType: EventType, _ rateValue: Float = 0, _ fromValue: Int = 0, _ toValue: Int = 0, onCompletion: @escaping () -> Void) {
        let event = DZEventCollector.shared.createEvent(eventType: eventType)
        DZEventCollector.shared.triggerMessage(event: event, rateValue: rateValue, fromValue: fromValue, toValue: toValue){ (res) in
            onCompletion()
        }
    }
    
    func sendMessageForEvent(_ eventType: EventType, _ rateValue: Float = 0, _ fromValue: Int = 0, _ toValue: Int = 0, onCreate: ((DZEvent) -> Void)?, onCompletion: @escaping () -> Void) {
        let event = DZEventCollector.shared.createEvent(eventType: eventType)
        onCreate?(event)
        DZEventCollector.shared.triggerMessage(event: event, rateValue: rateValue, fromValue: fromValue, toValue: toValue){ (res) in
            onCompletion()
        }
    }
    
    
    
    func triggerNotImplemented(_ onSent: (() -> Void)?) {
        print("Not implemented")
    }
    
    
    func triggerDatazoomLoaded(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.datazoomLoaded){
            onSent?()
        }
    }
    
    func triggerError(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.error, playHeadPos){
            onSent?()
        }
    }
    
    func triggerMilestone(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.milestone, playHeadPos){
            onSent?()
        }
    }
    
    func triggerRenditionChange(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.renditionChange, playHeadPos){
            onSent?()
        }
    }
    
    func triggerMediaRequest(_ onBeforeSend: (() -> Void)? = nil, _ onCreate: ((DZEvent) -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content

        sendMessageForEvent(.mediaRequest, 0.0, 0, 0, onCreate: onCreate, onCompletion: {
            onSent?()
        })
    }
    
    func triggerMediaLoaded(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.mediaLoaded){
            onSent?()
        }
    }
    
    func triggerPlaybackReady(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.playbackReady){
            onSent?()
        }
    }
    
    func triggerPlaybackStart(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.playbackStart){
            onSent?()
        }
    }
    
    func triggerPlay(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.play, playHeadPos){
            onSent?()
        }
    }
    
    func triggerPlaying(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.playing, playHeadPos){
            onSent?()
        }
    }
    
    func triggerSeekStart(_ playHeadPos: Float,_ fromValue: Int,_ toValue: Int, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.seekStart, playHeadPos, fromValue, toValue){
            onSent?()
        }
    }
    
    func triggerSeekEnd(_ playHeadPos: Float,_ fromValue: Int,_ toValue: Int, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.seekEnd, playHeadPos, fromValue, toValue){
            onSent?()
        }
    }
    
    func triggerPlaybackComplete(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.playbackComplete, playHeadPos){
            onSent?()
        }
    }
    
    func triggerHeartbeat(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.heartbeat, playHeadPos){
            onSent?()
        }
    }
    
    func triggerBufferStart(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.bufferStart){
            onSent?()
        }
    }
    
    func triggerBufferEnd(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.bufferEnd){
            onSent?()
        }
    }
    
    func triggerStallStart(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.stallStart){
            onSent?()
        }
    }
    
    func triggerStallEnd(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.stallEnd){
            onSent?()
        }
    }
    
    func triggerPause(_ playHeadPos: Float, _ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.pause, playHeadPos){
            onSent?()
        }
    }
    
    func triggerResume(_ playHeadPos: Float,_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .content
        sendMessageForEvent(.resume, playHeadPos){
            onSent?()
        }
    }
    
    
    func triggerNetworkTypeChange(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.networkTypeChange){
            onSent?()
        }
    }
        
    func triggerNetworkTimed(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.networkTimed){
            onSent?()
        }
    }
    
    func triggerVolumeChange(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.volumeChange){
            onSent?()
        }
    }
    
    func triggerMute(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.mute){
            onSent?()
        }
    }
    
    func triggerUnmute(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.unmute){
            onSent?()
        }
    }
    
    func triggerResize(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.resize){
            onSent?()
        }
    }
    
    func triggerFullScreen(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.fullscreen){
            onSent?()
        }
    }
    
    func triggerExitFullScreen(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.exitFullscreen){
            onSent?()
        }
    }
    
    func triggerAudioTrackChanged(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.audioTrackChanged){
            onSent?()
        }
    }
    
    func triggerQualityChangeRequest(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.qualityChangeRequest){
            onSent?()
        }
    }
    
    func triggerSubtitleChange(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.subtitleChange){
            onSent?()
        }
    }
    
    func triggerCastStart(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.castStart){
            onSent?()
        }
    }
    
    func triggerCastEnd(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.castEnd){
            onSent?()
        }
    }
    
    func triggerPlayBtn(_ onBeforeSend: (() -> Void)? = nil, _ onSent: (() -> Void)? = nil){
        onBeforeSend?()
        DZEventCollector.shared.video.mediaType = .none
        sendMessageForEvent(.playBtn){
            onSent?()
        }
    }

}
