//
//  DZEventResponse.swift
//  GoldCollectorTvOS
//
//  Created by Momcilo Stankovic on 03/12/2020.
//  Copyright © 2020 RedCellApps. All rights reserved.
//


import Foundation

class ItemResponse: Codable {
    var event_id: String?
    var status: String?
    var status_code: HTTPStatusCode?
//    var error: Error?
}

class DZEventResponse: Codable {
    var status: String?
    var status_code: Int?
    var message: String?
    var has_errors: Bool?
    var item_count: Int?
    var items: [ItemResponse] = []
}

enum HTTPStatusCode: Int, Error, Codable {
    case ok = 200
    case badRequest = 400
    case internalServerError = 500
    case serviceUnavailable = 503
    
    var isRetriable: Bool {
        return self == .serviceUnavailable
    }
}
