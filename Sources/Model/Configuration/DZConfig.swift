//
//  ConfigJSON.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 6.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public struct DZConfig {
    public let events : DZConfigEvents?
    public let eventsV3 : DZConfigEventTypes?
    public let dataCollector : DZConfigDataCollector?
    public let customerCode : String?
    public let connectorList : String?
    public let configurationId : String?
    public let dzApp : String?
    public let oauthToken : String?
    public let broker : DZConfigBroker?
}

extension DZConfig: Decodable {
    enum CodingKeys: String, CodingKey {
        case events = "events"
        case eventsV3 = "events_v3"
        case dataCollector = "data_collector"
        case customerCode = "customer_code"
        case connectorList = "connector_list"
        case configurationId = "configuration_id"
        case dzApp = "dz_app"
        case oauthToken = "oauth_token"
        case broker = "broker_url"
    }
    
    private static func getTypesForTypeData(_ types: [DZConfigTypeElementV3], _ type: String) -> [DZConfigTypeElement] {
        var retVal: [DZConfigTypeElement] = []
        let naItems = types.filter{ $0.mediaTypes == nil || $0.mediaTypes?.count == 0 || $0.mediaTypes?.contains("not_appl") == true || $0.mediaTypes?.contains("na") == true  }.map { $0.name }.compactMap { $0 }
        let items = types.filter{ $0.mediaTypes?.contains(type) == true }.map { $0.name }.compactMap { $0 }
        
        naItems.forEach { item in
            retVal.append(DZConfigTypeElement(item))
        }
        
        items.forEach { item in
            retVal.append(DZConfigTypeElement(item))
        }
        
        return retVal
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        events = try values.decodeIfPresent(DZConfigEvents.self, forKey: .events)
        //eventsV3 = try values.decodeIfPresent(DZConfigEventTypes.self, forKey: .eventsV3)
        let typesV3 = try values.decodeIfPresent([DZConfigTypeElementV3].self, forKey: .eventsV3)
        var eV3 = DZConfigEventTypes()
        if typesV3 != nil {
            eV3.typesContent = DZConfig.getTypesForTypeData(typesV3!, "content")
            eV3.typesAd = DZConfig.getTypesForTypeData(typesV3!, "ad")
        }
        self.eventsV3 = eV3
        
        dataCollector = try values.decodeIfPresent(DZConfigDataCollector.self, forKey: .dataCollector)
        customerCode = try values.decodeIfPresent(String.self, forKey: .customerCode)
        connectorList = try values.decodeIfPresent(String.self, forKey: .connectorList)
        configurationId = try values.decodeIfPresent(String.self, forKey: .configurationId)
        dzApp = try values.decodeIfPresent(String.self, forKey: .dzApp)
        oauthToken = try values.decodeIfPresent(String.self, forKey: .oauthToken)
        broker = try values.decodeIfPresent(DZConfigBroker.self, forKey: .broker)
    }
    
    
}
