//
//  DZEventNetworkDetails.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
#if !os(macOS)
import UIKit
#endif

public class DZEventNetworkDetails {
    public var asn : String = String()
    public var org : String = String()
    public var isp : String = String()
    public var connectionType : String = String()
    
    var configuration: [CodingKeys] = []
    
    public func initFromConfiguration(_ net: DZConfigNetworkDetails?) {
        if let n = net {
            self.asn = n.asn ?? "NA"
            self.org = n.org ?? "NA"
            self.isp = n.isp ?? "NA"
        }
    }    
}

extension DZEventNetworkDetails : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case asn = "asn"
        case org = "asn_org"
        case isp = "isp"
        case connectionType = "connection_type"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.asn) {
            try container.encode(asn, forKey: .asn)
        }
        if configuration.contains(.org) {
            try container.encode(org, forKey: .org)
        }
        if configuration.contains(.isp) {
            try container.encode(isp, forKey: .isp)
        }
        if configuration.contains(.connectionType) {
            try container.encode(connectionType, forKey: .connectionType)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}

