//
//  DZEventVideo.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventVideo {
    public var title: String = String()
    public var source: String = String()
    public var mediaType: VideoType = VideoType.none
    public var duration: Float = Float()
    public var frameRate: Float = Float()
    public var playerHeight: Int = Int()
    public var playerWidth: Int = Int()
    
    var configuration: [CodingKeys] = []
}

extension DZEventVideo : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case title = "title"
        case source = "source"
        case mediaType = "media_type"
        case duration = "duration_sec"
        case frameRate = "frame_rate"
        case playerHeight = "player_height"
        case playerWidth = "player_width"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.title) {
            try container.encode(title, forKey: .title)
        }
        if configuration.contains(.source) {
            try container.encode(source, forKey: .source)
        }
        if configuration.contains(.mediaType) {
            try container.encode(mediaType.key, forKey: .mediaType)
        }
        if configuration.contains(.duration) {
            try container.encode(duration, forKey: .duration)
        }
        if configuration.contains(.frameRate) {
            try container.encode(frameRate, forKey: .frameRate)
        }
        if configuration.contains(.playerHeight) {
            try container.encode(playerHeight, forKey: .playerHeight)
        }
        if configuration.contains(.playerWidth) {
            try container.encode(playerWidth, forKey: .playerWidth)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}
