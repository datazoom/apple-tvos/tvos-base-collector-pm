//
//  D.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventPage {
    public var dzSdkVer: String = String()
    public var appName: String = String()
    
    public init () {
        self.appName = Bundle.main.bundleIdentifier!
        self.dzSdkVer = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0"
    }
    var configuration: [CodingKeys] = []
}

extension DZEventPage : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case dzSdkVer = "dz_sdk_version"
        case appName = "site_domain"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.dzSdkVer) {
            try container.encode(dzSdkVer, forKey: .dzSdkVer)
        }
        if configuration.contains(.appName) {
            try container.encode(appName, forKey: .appName)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}
