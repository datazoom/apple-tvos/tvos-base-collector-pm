//
//  DZEventCustomMetadata.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 16.12.21..
//  Copyright © 2021 Adhoc Technologies. All rights reserved.
//

import Foundation

public class DZEventCustomMetadata {
    public var customMetadata: [String: Any] = [:]
    public var playerMetadata: [String: Any] = [:]
    public var sessionMetadata: [String: Any] = [:]
}

extension DZEventCustomMetadata : Encodable {
    enum CodingKeys: String, CodingKey {
        case playerCustomMetadata = "player"
        case sessionCustomMetadata = "session"
    }
    
    public func encode(to encoder: Encoder) throws {
//        var baseContainer = encoder.container(keyedBy: CodingKeys.self)
//        let customMetadataEncoded = DictionaryEncoder.encode(customMetadata)
//        try self.customMetadataEncoded.encode(to: baseContainer)
//
//        let playerEncoder = baseContainer.superEncoder(forKey: .playerCustomMetadata)
//        try self.playerCustomMetadata.encode(to: playerEncoder)
//
//        let sessionEncoder = baseContainer.superEncoder(forKey: .playerCustomMetadata)
//        try self.sessionCustomMetadata.encode(to: sessionEncoder)
    }
}
