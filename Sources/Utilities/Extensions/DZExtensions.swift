//
//  DZExtensions.swift
//  DataZoom
//
//  Created by Momcilo Stankovic on 05/04/21.
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
#if !os(macOS)
import UIKit
#endif

public extension Data {
    func toString() -> String {
        return String(data: self, encoding: .utf8)!
    }
    
}

#if !os(macOS)
public extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
#endif

var Timestamp: String {
    let timestamp = UInt64(floor(NSDate().timeIntervalSince1970 * 1000))
    let finalTS = String(timestamp)
    return finalTS
}

public extension Date {
    func getTimestamp() -> UInt64 {
        return UInt64(floor(NSDate().timeIntervalSince1970 * 1000))
    }
}

public extension String {
    func toData() -> String {
        let data = self.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>] {
                print("Array JSON :: \(jsonArray)")
            }
            
        }catch let error as NSError {
            print(error)
        }
        return String(data: data, encoding: .utf8)!
    }
}
